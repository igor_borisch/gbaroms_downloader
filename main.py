from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import requests

class GbaRomsDownloader():


    def __init__(self,number):
        self.page_number = number
        self.driver = webdriver.Chrome()
        self.link = 'http://gbaroms.ru/page/'
        self.game_index = 0
        self.download_button_index = 47


    def set_and_start(self):
        link = '{}{}'.format(self.link, self.page_number)
        isend = self.OpenSitePage(link)
        if isend == 1:
            self.shutdown()


    def OpenSitePage(self,link):
        #link = '{}{}'.format(self.link, self.page)
        driver = self.driver
        driver.get(link)
        response = requests.get(link)
        print(response.status_code)
        if response.status_code == 404:
            print('incorrect url, probably end of list')
            return 1
        else:
            self.GbaParseItems()


    def GbaParseItems(self):
        driver = self.driver
        game_list = driver.find_elements_by_xpath( "//a[@class='title']")
        count_of_games = len(game_list)
        game_list[self.game_index].send_keys(Keys.ENTER)
        self.download()
        self.game_index = self.game_index + 1
        if (self.game_index != count_of_games):
            try:
                self.set_and_start()
            except RecursionError:
                print('looks like you rich maximum recursion depth... try to change page number params(set it as last number of page in console) and start again')
        else:
            print('ALL DONE AT THE PAGE: ')
            print(self.page_number)
            self.page_number = int(self.page_number) + 1
            self.game_index = 0
            self.set_and_start()

        print(self.page_number)
        print('FINISH')



    def download(self):
        driver = self.driver
        refs_list = driver.find_elements_by_xpath('//a')
        download_button = refs_list[self.download_button_index]
        print(download_button)
        download_button.send_keys(Keys.ENTER)


    def shutdown(self):
        self.driver.close()


if __name__ == "__main__":
    print('number of page witch you want to start from: the last one is 77')
    number = input()
    DownloadBot = GbaRomsDownloader(number)
    DownloadBot.set_and_start()